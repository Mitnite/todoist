import {FormControl} from "@angular/forms";

export interface IUser {
    username: string;
    email: string;
    password: string;
    task: ITask[];
    category: ICategory[];
}

export interface ITask {
    id: number;
    description: string;
    category: ICategory | null;
    deadline: any;
    priority: IPriority;
    isDone: boolean;
}

export interface ICategory {
    id: number;
    name: string;
}

export interface IPriority {
    name: string;
}

export interface ISignUpForm {
    username: FormControl<string | null>;
    email: FormControl<string | null>;
    password: FormControl<string | null>;
}

export interface ISignInForm {
    email: FormControl<string | null>;
    password: FormControl<string | null>;
}

export interface IFiltersForm {
    dateFrom: FormControl<Date | string | null>;
    dateTo: FormControl<Date | string | null>;
    category: FormControl<ICategory | null>;
    priority: FormControl<IPriority | null>;
    isDone: FormControl<IIsDone | null>;
}

export interface IIsDone {
    name: string;
    state: boolean;
}

export interface IEditTask {
    deadline: FormControl<Date | string | null>;
    description: FormControl<string | null>;
    category: FormControl<ICategory | null>;
    priority: FormControl<IPriority | null>;
    id: FormControl<number | null>;
    isDone: FormControl<boolean | null>;
}

export interface IEditCategory {
    id: FormControl<number | null>;
    name: FormControl<string | null>;
}

export interface IOption {
    label: string;
    value: string;
}


