import {inject, Injectable} from "@angular/core";
import {
    CanActivateFn,
    Router,
} from "@angular/router";
import {CookieService} from "ngx-cookie-service";

@Injectable({
    providedIn: 'root'
})
class AuthorizationGuard {

    constructor(private router: Router, private cookie: CookieService) {}
    canActivate(): boolean  {
        if (this.cookie.get('userId')) {
            return true
        } else {
            this.router.navigateByUrl('/login');
            return false
        }
    }
}

export const IsAuthorizationGuard: CanActivateFn = (): boolean => {
    return inject(AuthorizationGuard).canActivate()
}

