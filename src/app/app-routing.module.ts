import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {SignInComponent} from "./pages/signin/signin.component";
import {SignUpComponent} from "./pages/signup/signup.component";
import {PageNotFoundComponent} from "./pages/page-not-found/page-not-found.component";
import {IsAuthorizationGuard} from "./guards/authorization.guard";

const routes: Routes = [
    {path: '', component: HomeComponent, canActivate: [IsAuthorizationGuard]},
    {path: 'login', component: SignInComponent},
    {path: 'registration', component: SignUpComponent},
    {path: '404', component: PageNotFoundComponent},
    {path: '**', component: PageNotFoundComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
