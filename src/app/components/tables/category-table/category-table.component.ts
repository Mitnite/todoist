import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  signal,
} from '@angular/core';
import {ICategory} from '../../../type';
import {CategoryService} from '../../../services/category.service';
import {TaskService} from '../../../services/task.service';
import {CATEGORY_LOCALIZATION} from './config/constants';
import {BehaviorSubject} from 'rxjs';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {DeletePopupComponent} from '../../modal-windows/delete-popup/delete-popup.component';
import {EditCategoryPopupComponent} from '../../modal-windows/edit-category-popup/edit-category-popup.component';

@Component({
  selector: 'category-table',
  templateUrl: './category-table.component.html',
  styleUrls: ['./category-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    ButtonModule,
    TableModule,
    DeletePopupComponent,
    EditCategoryPopupComponent,
  ],
  standalone: true,
})
export class CategoryTableComponent implements OnInit {
  data: ICategory[] = [];
  selectedCategory$ = new BehaviorSubject<ICategory | null>(null);

  isShowCreatePopup = signal(false); // Popup для создания Category
  isShowUpdatePopup = signal(false); // Popup для обновления Category
  isShowDeletePopup = signal(false); // Popup для удаления Category

  protected readonly CATEGORY_LOCALIZATION = CATEGORY_LOCALIZATION;

  constructor(
    private taskService: TaskService,
    private categoryService: CategoryService
  ) {
  }

  ngOnInit(): void {
    this.categoryService.categoryStream$.subscribe((categories: ICategory[]) => {
      this.data = categories;
    });
  }

  createCategory(newCategory: ICategory) {
    this.categoryService.createCategory(newCategory);
  }

  updateCategory(updateCategory: ICategory) {
    this.categoryService.updateCategory(updateCategory);
    this.taskService.updateCategory(this.data);
  }

  deleteCategory() {
    this.isShowDeletePopup.set(false);
    this.categoryService.deleteCategory(this.selectedCategory$.value!);
    this.taskService.updateCategory(this.data);
  }
}
