export const CATEGORY_LOCALIZATION = {
    ADD_CATEGORY_BUTTON_LABEL: 'Добавить категорию',

    TABLE_TH_CATEGORY_NAME: 'Название',
    TABLE_TH_REDACTION: 'Редактирование',
}
