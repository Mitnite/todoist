export const TASK_LOCALIZATION = {
    SEARCH_BUTTON_PLACEHOLDER: 'Поиск',
    SEARCH_ERROR_TEXT: 'К сожалению, не нашлось ни одной задачи 😒',
    ADD_TASK_BUTTON_LABEL: 'Добавить задачу',

    FILTER_OPEN_BUTTON_LABEL: 'Фильтры',
    FILTER_APPLY_BUTTON_LABEL: 'Применить',

    FILTER_DATE_FROM_LABEL: 'От',
    FILTER_DATE_TO_LABEL: 'До',
    FILTER_IS_DONE_LABEL: 'Выполнено',

    TABLE_TH_REDACTION: 'Редактирование',
};
