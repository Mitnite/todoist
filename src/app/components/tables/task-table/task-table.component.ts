import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ICategory, IFiltersForm, IPriority, ITask, IIsDone} from "../../../type";
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {done, GLOBAL_LOCALIZATION, priority} from "../../../config/constants";
import {TaskService} from "../../../services/task.service";
import {CategoryService} from "../../../services/category.service";
import {TASK_LOCALIZATION} from "./config/constants";
import {BehaviorSubject} from "rxjs";
import {ButtonModule} from "primeng/button";
import {InputTextModule} from "primeng/inputtext";
import {CalendarModule} from "primeng/calendar";
import {DropdownModule} from "primeng/dropdown";
import {AsyncPipe, NgIf} from "@angular/common";
import {TableModule} from "primeng/table";
import {CheckboxModule} from "primeng/checkbox";
import {TagModule} from "primeng/tag";
import {EditTaskPopupComponent} from "../../modal-windows/edit-task-popup/edit-task-popup.component";
import {DeletePopupComponent} from "../../modal-windows/delete-popup/delete-popup.component";

@Component({
    selector: 'task-table',
    templateUrl: './task-table.component.html',
    styleUrls: ['./task-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [
        FormsModule,
        ButtonModule,
        InputTextModule,
        ReactiveFormsModule,
        CalendarModule,
        DropdownModule,
        NgIf,
        TableModule,
        CheckboxModule,
        TagModule,
        EditTaskPopupComponent,
        DeletePopupComponent,
        AsyncPipe
    ],
    standalone: true
})
export class TaskTableComponent implements OnInit {
    data: ITask[] = [];
    private protectedData: ITask[] = [];
    category: ICategory[] = [];

    selectedTask$ = new BehaviorSubject<ITask | null>(null);
    searchInput: string | null = null;

    isShowCreatePopup$ = new BehaviorSubject<boolean>(false); // Popup для создания Task
    isShowUpdatePopup$ = new BehaviorSubject<boolean>(false); // Popup для обновления Task
    isShowDeletePopup$ = new BehaviorSubject<boolean>(false); // Popup для удаления Task
    isShowFilter$ = new BehaviorSubject<boolean>(false); // Выпадающий список фильтров

    filters: FormGroup = new FormGroup<IFiltersForm>({
        dateFrom: new FormControl(null),
        dateTo: new FormControl(null),
        category: new FormControl(null),
        priority: new FormControl(null),
        isDone: new FormControl({name: `Все`, state: false})
    });

    priority: IPriority[] = priority;

    done: IIsDone[] = done;
    protected readonly TASK_LOCALIZATION = TASK_LOCALIZATION;
    protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;

    constructor(private taskService: TaskService, private categoryService: CategoryService) {
    }

    ngOnInit(): void {
        this.taskService.taskStream$.subscribe((tasks: ITask[]) => {
            this.data = tasks;
            this.protectedData = tasks;
        });
        this.categoryService.categoryStream$.subscribe((categories: ICategory[]) => {
            this.category = categories;
        });
    }


    createTask(newTask: ITask) {
        this.taskService.createTask(newTask);
    }

    updateTask(editedTask: ITask) {
        this.taskService.updateTask(editedTask);
    }

    deleteTask() {
        this.isShowDeletePopup$.next(false);
        this.taskService.deleteTask(this.selectedTask$.value!);
    }

    getPriority(priority: string) {
        switch (priority) {
            case 'Низкий':
                return 'success';
            case 'Средний':
                return 'warning';
            case 'Высокий':
                return 'danger';
            default:
                return 'success';
        }
    }

    onSearch() {
        this.data = this.protectedData;
        this.data = this.data.filter((task: ITask) =>
            task.description.toLowerCase().indexOf(this.searchInput!.toLowerCase()) !== -1);
    }

    updateFilters() {
        this.searchInput = '';
        if (this.isShowFilter$.value) {
            this.filters.patchValue({
                dateFrom: null,
                dateTo: null,
                category: null,
                priority: null,
                isDone: {name: `Все`, state: false}
            });
        }
        this.isShowFilter$.next(!this.isShowFilter$.value);
        this.data = this.protectedData;
    }

    onSubmitFilter() {
        this.data = this.protectedData;

        if (this.filters.value.category) {
            this.data = this.data.filter((task: ITask) =>
                task.category?.id === this.filters.value.category?.id);
        }

        if (this.filters.value.priority) {
            this.data = this.data.filter((task: ITask) =>
                task.priority?.name === this.filters.value.priority?.name);
        }

        if (this.filters.value.dateFrom) {
            if (typeof this.filters.value.dateFrom !== 'string') {
                this.filters.patchValue({dateFrom: this.dateToString(this.filters.value.dateFrom)});
            }
            this.data = this.data.filter((task: ITask) =>
                this.reverseDateHandler(task.deadline) > this.reverseDateHandler(<string>this.filters.value.dateFrom));
        }

        if (this.filters.value.dateTo) {
            if (typeof this.filters.value.dateTo !== 'string') {
                this.filters.patchValue({dateTo: this.dateToString(this.filters.value.dateTo)});
            }
            this.data = this.data.filter((task: ITask) =>
                this.reverseDateHandler(task.deadline) < this.reverseDateHandler(<string>this.filters.value.dateTo));
        }

        if (this.filters.value.isDone?.name !== 'Все') {
            this.data = this.data.filter((task: ITask) =>
                this.filters.value.isDone?.state === task.isDone);
        }

    }

    private reverseDateHandler(date: string): string {
        let newDate: string[] = date.split(' ')[0].split('.');
        date = `${newDate[2]}.${newDate[1]}.${newDate[0]} ${date.split(' ')[1]}`;
        return date;
    }

    private dateToString(date: Date): string {
        const minutes: string = ('0' + date.getMinutes()).slice(-2);
        return `${date.toLocaleDateString('ru-RU')} ${date.getHours()}:${minutes}`;

    }

    isRowSelectable(id: number) {
        this.data.forEach((dataItem: ITask) => {
            if (dataItem.id === id) {
                this.updateTask(dataItem);
            }
        });

    }

}
