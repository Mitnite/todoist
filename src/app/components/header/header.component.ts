import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
} from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { IOption, IUser } from '../../type';
import { UsersService } from '../../services/users.service';
import { Router } from '@angular/router';
import { stateOptions } from '../../config/constants';
import { SelectButtonModule } from 'primeng/selectbutton';
import { FormsModule } from '@angular/forms';
import { AvatarModule } from 'primeng/avatar';
import { CommonModule } from '@angular/common';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import {HEADER_LOCALIZATION} from "./config/constants";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    SelectButtonModule,
    FormsModule,
    AvatarModule,
    InputTextModule,
    ButtonModule,
    CommonModule,
  ],
  standalone: true,
})
export class HeaderComponent {
  @Output() changeTable: EventEmitter<string> = new EventEmitter<string>();

  isShowDetails: boolean = false;

  stateOptions: IOption[] = stateOptions;

  value: string = 'task';

  protected readonly HEADER_LOCALIZATION = HEADER_LOCALIZATION;

  private readonly USER_ID: string;
  user: IUser;

  constructor(
    private router: Router,
    private CookieService: CookieService,
    private userLS: UsersService
  ) {
    this.USER_ID = this.CookieService.get('userId');
    this.user = this.userLS.getUserById(this.USER_ID);
  }

  logout() {
    this.CookieService.delete('userId');
    this.router.navigate(['/login']);
  }

}
