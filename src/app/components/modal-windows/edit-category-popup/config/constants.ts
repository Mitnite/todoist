export const EDIT_CATEGORY_LOCALIZATION = {
    CATEGORY_ERROR_TEXT: 'Введите название категории',
    CATEGORY_INVALID_TEXT: 'Категория уже существует',

    UPDATE_CATEGORY_TITLE: 'Обновить категорию',
    CREATE_CATEGORY_TITLE: 'Добавить категорию',

};
