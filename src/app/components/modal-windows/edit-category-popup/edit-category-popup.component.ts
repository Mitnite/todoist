import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ICategory, IEditCategory} from "../../../type";
import {EDIT_CATEGORY_LOCALIZATION} from './config/constants';
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {GLOBAL_LOCALIZATION} from "../../../config/constants";
import {CategoryService} from "../../../services/category.service";
import {BackdropComponent} from "../../backdrop/backdrop.component";
import {InputTextModule} from "primeng/inputtext";
import {NgClass} from "@angular/common";
import {ButtonModule} from "primeng/button";

@Component({
    selector: 'edit-category-popup',
    templateUrl: './edit-category-popup.component.html',
    styleUrls: ['./edit-category-popup.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [
        BackdropComponent,
        ReactiveFormsModule,
        InputTextModule,
        NgClass,
        ButtonModule
    ],
    standalone: true
})
export class EditCategoryPopupComponent implements OnInit {
    @Input() category: ICategory | null = null;
    @Input() isUpdate: boolean = false;

    @Output() deny: EventEmitter<void> = new EventEmitter<void>();
    @Output() editedCategory: EventEmitter<ICategory> = new EventEmitter<ICategory>();

    isValidCategory: boolean = false;

    check: ICategory[] = [];

    editCategory: FormGroup = new FormGroup<IEditCategory>({
        id: new FormControl(0),
        name: new FormControl(null, Validators.required)
    });

    protected readonly EDIT_CATEGORY_LOCALIZATION =  EDIT_CATEGORY_LOCALIZATION;
    protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;

    constructor(private categoryService: CategoryService) {
    }

    ngOnInit() {
        this.categoryService.categoryStream$.subscribe((categories: ICategory[]) => {
            this.check = categories;
        });
        if (this.category) {
            this.editCategory.patchValue({name: this.category.name, id: this.category.id});
        }
    }

    isControlInvalid(controlName: string): boolean {
        const control = this.editCategory.controls[controlName];
        return control.invalid && control.touched;
    }

    onSubmit() {
        const existingCategory: ICategory | undefined = this.check.find((cat: ICategory) => cat.name === this.editCategory.value.name);

        if (this.editCategory.invalid) {
            this.editCategory.controls['category'].markAsTouched();
            return;
        }

        if (existingCategory) {
            this.isValidCategory = true;
            return;
        } else {
            this.editedCategory.emit(this.editCategory.value);
            this.deny.emit();
        }
    }
}
