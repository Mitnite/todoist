import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {DELETE_LOCALIZATION} from "./config/constants";
import {ButtonModule} from "primeng/button";
import {BackdropComponent} from "../../backdrop/backdrop.component";

@Component({
    selector: 'delete-popup',
    templateUrl: './delete-popup.component.html',
    styleUrls: ['./delete-popup.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [
        ButtonModule,
        BackdropComponent
    ],
    standalone: true
})
export class DeletePopupComponent {

    @Input() value!: string;

    @Output() accept: EventEmitter<void> = new EventEmitter<void>();
    @Output() deny: EventEmitter<void> = new EventEmitter<void>();

    protected readonly DELETE_LOCALIZATION = DELETE_LOCALIZATION;
}
