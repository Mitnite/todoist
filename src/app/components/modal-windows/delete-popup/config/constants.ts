export const DELETE_LOCALIZATION = {
    DELETE_TEXT: 'Вы уверены, что хотите удалить',

    DENY_BUTTON_LABEL: 'Нет, отменить',
    ACCEPT_BUTTON_LABEL: 'Да, удалить',
};
