export const EDIT_TASK_LOCALIZATION = {
    DEADLINE_ERROR_TEXT: 'Введите актуальный срок выполнения задачи',
    DESCRIPTION_ERROR_TEXT: 'Введите описание задачи',

    UPDATE_TASK_TITLE: 'Обновить задачу',
    CREATE_TASK_TITLE: 'Добавить задачу',
};
