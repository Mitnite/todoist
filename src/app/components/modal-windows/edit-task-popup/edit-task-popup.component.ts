import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ICategory, IEditTask, IPriority, ITask} from "../../../type";
import {AbstractControl, FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {GLOBAL_LOCALIZATION, priority} from "../../../config/constants";
import {EDIT_TASK_LOCALIZATION} from "./config/constants";
import {BackdropComponent} from "../../backdrop/backdrop.component";
import {CalendarModule} from "primeng/calendar";
import {DropdownModule} from "primeng/dropdown";
import {NgClass, NgIf} from "@angular/common";
import {InputTextareaModule} from "primeng/inputtextarea";

@Component({
    selector: 'edit-task-popup',
    templateUrl: './edit-task-popup.component.html',
    styleUrls: ['./edit-task-popup.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [
        BackdropComponent,
        ReactiveFormsModule,
        CalendarModule,
        DropdownModule,
        NgClass,
        NgIf,
        InputTextareaModule
    ],
    standalone: true
})
export class EditTaskPopupComponent implements OnInit {

    @Input() task: ITask | null = null;
    @Input() isUpdate: boolean = false;
    @Input() category: ICategory[] = [];

    @Output() editedTask: EventEmitter<ITask> = new EventEmitter<ITask>();
    @Output() deny: EventEmitter<void> = new EventEmitter<void>();

    currentDate: Date = new Date();

    priority: IPriority[] = priority;

    editTask: FormGroup = new FormGroup<IEditTask>({
        deadline: new FormControl(null, Validators.required),
        description: new FormControl(null, Validators.required),
        category: new FormControl(null),
        priority: new FormControl({name: `Низкий`}),
        id: new FormControl(0),
        isDone: new FormControl(false)
    });

    protected readonly EDIT_TASK_LOCALIZATION = EDIT_TASK_LOCALIZATION;
    protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;

    ngOnInit() {
        if (this.task) {
            this.editTask.patchValue(this.task);
        }
    }

    isControlInvalid(controlName: string): boolean {
        const control: AbstractControl = this.editTask.controls[controlName];
        return control.invalid && control.touched;
    }

    isDateValid(): boolean {
        return (this.editTask.value.deadline < this.currentDate) && this.editTask.value.deadline !== null;
    }

    onSubmit() {
        const controls = this.editTask.controls;

        if (this.editTask.invalid) {
            Object.keys(controls)
                .forEach(controlName => controls[controlName].markAsTouched());
            return;
        }

        if (this.isDateValid()) {
            return;
        }

        const data = this.editTask.value.deadline;
        if (typeof data !== 'string' && data) {
            const minutes: string = ('0' + data.getMinutes()).slice(-2);
            this.editTask.patchValue({deadline: `${data.toLocaleDateString('ru-RU')} ${data.getHours()}:${minutes}`});
        }

        this.editedTask.emit(this.editTask.value);
        this.deny.emit();
    }

}
