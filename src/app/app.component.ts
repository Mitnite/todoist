import {Component} from '@angular/core';
import {PrimeNGConfig} from "primeng/api";
import translate from './ru.json';
@Component({
    selector: 'app-root',
    template: `<router-outlet></router-outlet>`,
})
export class AppComponent  {
    constructor(private config: PrimeNGConfig) {}

    ngOnInit() {
        this.config.setTranslation(translate.ru);
    }
}
