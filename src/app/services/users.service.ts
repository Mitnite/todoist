import {IUser} from "../type";
import {Injectable} from '@angular/core';
import {StorageService} from "./localStorage.service";

@Injectable({
    providedIn: 'root'
})

export class UsersService {
    private name: string = 'signupUsers';
    private readonly users: IUser[] = [];

    constructor(private StorageService: StorageService) {
        const storage: string | null = this.StorageService.getItem(this.name);
        if (storage) {
            this.users = JSON.parse(storage);
        }
    }

    signUp(item: IUser): boolean {
        const existingUser = this.users.find((user) => user.email === item.email);

        if (existingUser) {
            return false;

        } else {
            this.users.push({username: item.username, email: item.email, password: item.password, task: [], category: []});
            this.StorageService.setItem(this.name, this.users);
            return true;
        }
    }

    signIn(item: IUser): boolean {
        const accept = this.users.find((user) => (user.email === item.email) && (user.password === item.password));
        return !!accept;
    }

    getUserById(id: string): IUser {
        return this.users.find((user) => (user.email === id)) || {username: '', password: '', email: '', task: [], category: []};
    }

    updateUser(){
        this.StorageService.setItem(this.name, this.users);
    }
/*

    /!* Удалить всех пользователей *!/
    clear() {
        this.service.removeItem(this.name);
    }
*/
}
