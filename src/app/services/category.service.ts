import {Injectable} from "@angular/core";
import {ICategory, IUser} from "../type";
import {BehaviorSubject} from "rxjs";
import {CookieService} from "ngx-cookie-service";
import {UsersService} from "./users.service";

@Injectable({
    providedIn: 'root'
})


export class CategoryService {
    private user: IUser;
    categoryStream$: BehaviorSubject<ICategory[]>;

    constructor(private cookie: CookieService, private userLS: UsersService) {
        this.user = this.userLS.getUserById(this.cookie.get('userId'));
        this.categoryStream$ = new BehaviorSubject(this.user.category);
    }

    createCategory(newCategory: ICategory) {
        newCategory.id = this.user.category && this.user.category.length > 0
            ? this.user.category[this.user.category.length - 1].id + 1
            : (Math.floor(Math.random() * 98) + 11 * 11000);
        this.user.category.push(newCategory);
        this.saveLocalStorage();
    }

    updateCategory(editedCategory: ICategory) {
        this.user.category.forEach((categoryItem: ICategory) => {
            if (categoryItem.id === editedCategory.id) {
                categoryItem.name = editedCategory.name;
            }
        });
        this.saveLocalStorage();
    }

    deleteCategory(deletedCategory: ICategory) {
        this.user.category.forEach((categoryItem: ICategory, index: number) => {
            if (categoryItem.id === deletedCategory.id) {
                this.user.category.splice(index, 1);
            }
        });
        this.saveLocalStorage();
    }

    protected saveLocalStorage() {
        this.categoryStream$.next(this.user.category);
        this.userLS.updateUser();
    }
}
