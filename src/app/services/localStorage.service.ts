import { Injectable } from '@angular/core';
import {IUser} from "../type";

@Injectable({ providedIn: 'root' })
export class StorageService {
    setItem(key: string, value: IUser[]) {
        localStorage.setItem(key, JSON.stringify(value));
    }
    getItem(key: string): any {
        return localStorage.getItem(key);
    }
    removeItem(key: string) {
        localStorage.removeItem(key);
    }
    clear(): void {
        localStorage.clear();
    }
}
