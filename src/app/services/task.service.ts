import { ICategory, ITask, IUser } from '../type';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UsersService } from './users.service';
import { CookieService } from 'ngx-cookie-service';
import {cloneDeep} from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  private user: IUser;
  taskStream$: BehaviorSubject<ITask[]>;

  constructor(private cookie: CookieService, private userLS: UsersService) {
    this.user = this.userLS.getUserById(this.cookie.get('userId'));
    this.taskStream$ = new BehaviorSubject(this.user.task);
  }

  createTask(newTask: ITask) {
    newTask.id =
      this.user.task && this.user.task.length > 0
        ? this.findId(this.user.task) + 1
        : Math.floor(Math.random() * 98) + 11 * 1000;
    this.user.task.push(newTask);
    this.saveLocalStorage();
  }

  updateTask(editedTask: ITask) {
    this.user.task.forEach((taskItem: ITask) => {
      if (taskItem.id === editedTask.id) {
        Object.assign(taskItem, editedTask);
      }
    });
    this.saveLocalStorage();
  }

  deleteTask(deletedTask: ITask) {
    this.user.task.forEach((taskItem: ITask, index: number) => {
      if (taskItem.id === deletedTask.id) {
        this.user.task.splice(index, 1);
      }
    });
    this.saveLocalStorage();
  }

  updateCategory(category: ICategory[]) {
    this.user.task.forEach((taskItem: ITask) => {
      const finder = category.find(
        (categoryItem: ICategory) => categoryItem.id === taskItem.category?.id
      );
      if (finder) {
        taskItem.category!.name = finder.name;
      } else {
        taskItem.category = null;
      }
    });
    this.saveLocalStorage();
  }

  protected saveLocalStorage() {
    this.user.task = this.sort(this.user.task);
    this.taskStream$.next(this.user.task);
    this.userLS.updateUser();
  }

  protected findId(arr: any[]) {
    let maxObj = arr.reduce((max, obj) => {
      return obj.id > max.id ? obj : max;
    });
    return maxObj.id;
  }

  protected sort(task: ITask[]) {
    let cloned: ITask[] = cloneDeep(task);

    cloned = this.reverseDateHandler(cloned);

    task = cloneDeep(cloned).sort((a: ITask, b: ITask) =>
      a.deadline > b.deadline ? 1 : -1
    );
    return this.reverseDateHandler(task);
  }

  protected reverseDateHandler(oldData: ITask[]): ITask[] {
    oldData.forEach((dataItem: ITask) => {
      let newDate = dataItem.deadline.split(' ')[0].split('.');
      dataItem.deadline = `${newDate[2]}.${newDate[1]}.${newDate[0]} ${
        dataItem.deadline.split(' ')[1]
      }`;
    });
    return oldData;
  }
}
