import {IIsDone, IOption, IPriority} from "../type";

export const priority: IPriority[] = [
    {name: `Низкий`},
    {name: 'Средний'},
    {name: 'Высокий'}
];

export const done: IIsDone[] = [
    {name: `Все`, state: false},
    {name: 'Выполнено', state: true},
    {name: 'Не выполнено', state: false}
];

export const stateOptions: IOption[] = [
    {label: 'Задачи', value: 'task'},
    {label: 'Категории', value: 'category'}
];

export const GLOBAL_LOCALIZATION = {
    DENY_BUTTON_LABEL: 'Отменить',
    CREATE_BUTTON_LABEL: 'Добавить',
    UPDATE_BUTTON_LABEL: 'Обновить',

    FORM_DEADLINE_LABEL: 'Срок выполнения',
    FORM_CATEGORY_LABEL: 'Категория',
    FORM_PRIORITY_LABEL: 'Приоритет',
    FORM_DESCRIPTION_LABEL: 'Описание',
    FORM_EMAIL_LABEL: 'Почта',
    FORM_PASSWORD_LABEL: 'Пароль',

    EMAIL_ERROR_TEXT: 'Введите Email в формате: a@b.c',
    PASSWORD_ERROR_TEXT: 'Введите пароль',
    REGISTRATION_BUTTON_LABEL: 'Регистрация',

};
