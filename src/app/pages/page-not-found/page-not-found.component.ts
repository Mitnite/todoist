import {ChangeDetectionStrategy, Component} from '@angular/core';
import {PAGE404_LOCALIZATION} from "./config/constants";
import {ButtonModule} from "primeng/button";
import {RouterLink} from "@angular/router";

@Component({
    selector: 'app-page-not-found',
    templateUrl: './page-not-found.component.html',
    styleUrls: ['./page-not-found.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [
        ButtonModule,
        RouterLink
    ],
    standalone: true
})
export class PageNotFoundComponent {
    protected readonly PAGE404_LOCALIZATION = PAGE404_LOCALIZATION;
}
