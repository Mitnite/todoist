export const PAGE404_LOCALIZATION = {
    NUMBER_TEXT: '404',
    PAGE_ERROR_TEXT: 'Извините, страница, которую вы ищете, не существует.',
    BACK_BUTTON_LABEL: 'Вернуться',
}
