import {ChangeDetectionStrategy, Component} from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {UsersService} from "../../services/users.service";
import {Router, RouterLink} from "@angular/router";
import {CookieService} from "ngx-cookie-service";
import {ISignInForm} from "../../type";
import {SIGNIN_LOCALIZATION} from "./config/constants";
import {GLOBAL_LOCALIZATION} from "../../config/constants";
import {NgClass, NgIf} from "@angular/common";
import {InputTextModule} from "primeng/inputtext";
import {PasswordModule} from "primeng/password";
import {ButtonModule} from "primeng/button";

@Component({
    selector: 'signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [
        ReactiveFormsModule,
        NgClass,
        InputTextModule,
        PasswordModule,
        ButtonModule,
        RouterLink,
        NgIf
    ],
    standalone: true
})
export class SignInComponent {

    isDataValid: boolean = true;

    signInForm: FormGroup = new FormGroup<ISignInForm>({
        password: new FormControl(null, Validators.required,),
        email: new FormControl(null, [Validators.required, Validators.email])
    });

    protected readonly SIGNIN_LOCALIZATION = SIGNIN_LOCALIZATION;
    protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;

    constructor(private router: Router, private CookieService: CookieService, private userLS: UsersService) {
    }

    isControlInvalid(controlName: string): boolean {
        const control = this.signInForm.controls[controlName];
        return control.invalid && control.touched;
    }

    onSubmit() {
        const controls = this.signInForm.controls;

        if (this.signInForm.invalid) {
            Object.keys(controls)
                .forEach(controlName => controls[controlName].markAsTouched());
            return;
        }

        if (this.userLS.signIn(this.signInForm.value)) {
            this.CookieService.set('userId', this.signInForm.value.email);
            this.router.navigate(['/']);
        } else {
            this.isDataValid = false;
        }
    }

}
