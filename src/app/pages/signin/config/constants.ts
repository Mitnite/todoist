export const SIGNIN_LOCALIZATION = {
    TITLE: 'Вход Todoist',

    VALIDATION_ERROR_TEXT: 'Неверный Email или пароль',

    LOGIN_BUTTON_LABEL: 'Войти в аккаунт',

    REGISTRATION_TEXT: 'Нет аккаунта?',
};
