export const SIGNUP_LOCALIZATION = {
    TITLE: 'Регистрация Todoist',

    NAME_ERROR_TEXT:  'Введите имя',
    EMAIL_VALID_TEXT:  'Данный Email уже используется',

    USERNAME_LABEL:  'Имя',

    BACK_BUTTON_LABEL:  'Назад',

};
