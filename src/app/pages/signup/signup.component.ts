import {ChangeDetectionStrategy, Component} from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {UsersService} from "../../services/users.service";
import {Router, RouterLink} from "@angular/router";
import {CookieService} from "ngx-cookie-service";
import {ISignUpForm} from "../../type";
import {SIGNUP_LOCALIZATION} from "./config/constants";
import {GLOBAL_LOCALIZATION} from "../../config/constants";
import {NgClass, NgIf} from "@angular/common";
import {InputTextModule} from "primeng/inputtext";
import {PasswordModule} from "primeng/password";
import {ButtonModule} from "primeng/button";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgClass,
    InputTextModule,
    ReactiveFormsModule,
    NgIf,
    PasswordModule,
    ButtonModule,
    RouterLink
  ],
  standalone: true
})

export class SignUpComponent {

  isEmailValid: boolean = true;

  protected readonly SIGNUP_LOCALIZATION = SIGNUP_LOCALIZATION;
  protected readonly GLOBAL_LOCALIZATION = GLOBAL_LOCALIZATION;

  signUpForm: FormGroup = new FormGroup<ISignUpForm>({
    password: new FormControl(null, [Validators.required]),
    email: new FormControl(null, [Validators.required, Validators.email]),
    username: new FormControl(null, [Validators.required])
  });

  constructor(private router: Router, private CookieService: CookieService, private userLS: UsersService) {
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.signUpForm.controls[controlName];
    return control.invalid && control.touched;
  }

  onSubmit() {
    const controls = this.signUpForm.controls;

    if (this.signUpForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());

      return;
    }

    if (this.userLS.signUp(this.signUpForm.value)) {
      this.CookieService.set('userId', this.signUpForm.value.email);
      this.router.navigate(['/']);
    } else {
      this.isEmailValid = false;
    }

  }
}
