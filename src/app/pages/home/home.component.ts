import {ChangeDetectionStrategy, Component} from '@angular/core';
import {HeaderComponent} from "../../components/header/header.component";
import {TaskTableComponent} from "../../components/tables/task-table/task-table.component";
import {CategoryTableComponent} from "../../components/tables/category-table/category-table.component";
import {NgIf} from "@angular/common";

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [
        HeaderComponent,
        TaskTableComponent,
        CategoryTableComponent,
        NgIf
    ],
    standalone: true
})
export class HomeComponent {
    value: string = 'task';
}
